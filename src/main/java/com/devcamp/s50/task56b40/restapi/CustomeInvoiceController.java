package com.devcamp.s50.task56b40.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class CustomeInvoiceController {
     @GetMapping("/invoices")
     public ArrayList<Invoice> getListInvoice(){
          //khởi tạo 3 đối tượng 
          Customer customer1 = new Customer(1, "Loc", 20);
          Customer customer2 = new Customer(2, "Cam", 30);
          Customer customer3 = new Customer(3, "Tai", 40);

          //in 3 đối tượng ra console
          System.out.println((customer1.toString()));
          System.out.println((customer2.toString()));
          System.out.println((customer3.toString()));
          
          Invoice invoice1 = new Invoice(1, customer1, 20000);
          Invoice invoice2 = new Invoice(2, customer1, 30000);
          Invoice invoice3 = new Invoice(3, customer1, 40000);

          //in đối tượng ra console
          System.out.println((invoice1.toString()));
          System.out.println((invoice2.toString()));
          System.out.println((invoice3.toString()));
          
          //tạo danh sách lớp 
          ArrayList<Invoice> invoices = new ArrayList<>();

          //thêm lần lượt các đối tượng hóa vào ArrayList
          invoices.add(invoice1);
          invoices.add(invoice2);
          invoices.add(invoice3);

          return invoices;
     }
}
